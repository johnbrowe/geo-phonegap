document.addEventListener("deviceready", readySteadyCook, false);

function readySteadyCook(){

    $( document ).ready(function() {

        /*******************/
        /* GEOCODER        */
        /*******************/
        var geocoder;
        var address;
        
        function getCoord( address ){
            geocoder.geocode( { 'address': address}, function(results, status) {
                var lat = results[0].geometry.location.lat();
                var lon = results[0].geometry.location.lng();

                console.log(lat);
                console.log(lon);

                $('#lat').text(lat);
                $('#lon').text(lon);
            });
        }
        
        $('#location').click(function(){
            address = $('#address').val();
            getCoord( address );
            alert("Search");
        });

        $('#start').click(function(){
            var startStop = ($('#start')[0].innerHTML);

            if(startStop == "Start"){
                $('#start').text('Stop');
                onDeviceReady("Start");
            } else {
                $('#start').text('Start');
                onDeviceReady("Stop");
            }
        });

        $('#range').click(function(){
            console.log("click");
        });


        geocoder = new google.maps.Geocoder();

        
        /*******************/
        /* GEOLOCATION     */
        /*******************/

        var watchID = null;

        // device APIs are available
        //
        function onDeviceReady(toggle) {
            // Throw an error if no update is received every 30 seconds
            var options;

            if(toggle == "Start")
            {
                options = { timeout: 5000 };
                watchID = navigator.geolocation.watchPosition(onSuccess, onError, options);
            } else {
                watchID = navigator.geolocation.clearWatch(watchID);
            }
        }

        // onSuccess Geolocation
        //
        function onSuccess(position) {

            var moverLat = position.coords.latitude;
            var moverLon = position.coords.longitude;

            var destLat =  $('#lat')[0].innerHTML;
            var destLon =  $('#lon')[0].innerHTML;
            var distance = findDistance(moverLat, moverLon, destLat, destLon);

            alert("hey");

            $('#dist').text(distance);

            /* Calculate distance */

            if(distance < 5){
                // The onDeviceready should stop
                // Send notification
                alert("Message sent");
                $('#destReach').text("You have reached the destination");
            }

        }


        // onError Callback receives a PositionError object
        //
        function onError(error) {
            alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
        }


        /*********************/
        /* Haversine formula */
        /*********************/

        var earthRadius = 6373;

        function findDistance(userLat,userLong,eventLat,eventLong)
        {
            var lat1, lon1, lat2, lon2, dlat, dlon, a, c, distanceKm, kmRound;

            // convert coordinates to radians
            lat1 = deg2rad(userLat);
            lon1 = deg2rad(userLong);
            lat2 = deg2rad(eventLat);
            lon2 = deg2rad(eventLong);

            // find the differences between the coordinates
            dlat = lat2 - lat1;
            dlon = lon2 - lon1;

            // calculate the distance with this form
            a  = Math.pow(Math.sin(dlat/2),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2);
            c  = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a)); // great circle distance in radians
            distanceKm = c * earthRadius;

            // round the results down to the nearest 1/1000
            kmRound = round(distanceKm);
            //display value
            return kmRound;
        }

        // convert degrees to radians
        function deg2rad(deg)
        {
            rad = deg * Math.PI/180; // radians = degrees * pi/180
            return rad;
        }

        // round to the nearest 1/1000
        function round(x)
        {
            return Math.round( x * 1000) / 1000;
        }
    });

}


