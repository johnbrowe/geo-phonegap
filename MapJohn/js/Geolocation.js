var geocoder = new google.maps.Geocoder();

var some = $('.location').text();


var sos = geocoder.geocode({ 'address': 'Brydes Alle' + ', denmark' });


//var currlat = localStorage.getItem('yourLatitude');
//var currlon = localStorage.getItem('yourLongitude');

/*$('#location').click(function () {
    alert("kjasfl");

    var some = $(this);
    geocoder.geocode({ 'address': some.text() + ', denmark' }, function (results, status) {
        eventPos = results[0].geometry.location;

        //distance = (google.maps.geometry.spherical.computeDistanceBetween(userPos, eventAddress)) * (0.001);
        var distance = findDistance(eventPos.nb, eventPos.ob, currlat, currlon);
        some.append("<p class='latitude' style='display: none'>" + eventPos.nb + "</p>");
        some.append("<p class='longitude' style='display: none'>" + eventPos.ob + "</p>");
        some.append("<p class='distance'>" + distance + " km" +"</p>");

    });
});*/
/*
$('#all').click(function () {
    $('.distance').each(function () {
        $(this).parentsUntil("ul").show();
    });
});

$('#two').click(function () {
    $('.distance').each(function () {
        $(this).parentsUntil("ul").show();

        var thisEl = $(this);
        var distance = parseFloat($(this).text());
        if (distance >= 2) {
            $(this).parentsUntil("ul").hide();
        }
    });
});

$('#five').click(function () {
    $('.distance').each(function () {
        $(this).parentsUntil("ul").show();

        var thisEl = $(this);
        var distance = parseFloat($(this).text());
        if (distance >= 5) {
            $(this).parentsUntil("ul").hide();
        }
    });
});

$('#five').click(function () {
    $('.distance').each(function () {
        $(this).parentsUntil("ul").show();
        
        var thisEl = $(this);
        var distance = parseFloat($(this).text());
        if (distance >= 5)
        {
            $(this).parentsUntil("ul").hide();
        }
    });
});

$('#ten').click(function () {
    $('.distance').each(function () {
        $(this).parentsUntil("ul").show();
        var thisEl = $(this);
        var distance = parseFloat($(this).text());
        if (distance > 10) {
            $(this).parentsUntil("ul").hide();
        }
    });
});

var earthRadius = 6373; // mean radius of the earth (km) at 39 degrees from the equator
*/
/* main function */
function findDistance(userLat,userLong,eventLat,eventLong) 
{
    var lat1, lon1, lat2, lon2, dlat, dlon, a, c, distanceKm, kmRound;
		
    // convert coordinates to radians
    lat1 = deg2rad(userLat);
    lon1 = deg2rad(userLong);
    lat2 = deg2rad(eventLat);
    lon2 = deg2rad(eventLong);
		
    // find the differences between the coordinates
    dlat = lat2 - lat1;
    dlon = lon2 - lon1;
		
    // calculate the distance with this form
    a  = Math.pow(Math.sin(dlat/2),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2);
    c  = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a)); // great circle distance in radians
    distanceKm = c * earthRadius;
		
    // round the results down to the nearest 1/1000
    kmRound = round(distanceKm);
    //display value
    return kmRound;
}
	
// convert degrees to radians
function deg2rad(deg) 
{
    rad = deg * Math.PI/180; // radians = degrees * pi/180
    return rad;
}
	
	
// round to the nearest 1/1000
function round(x) 
{
    return Math.round( x * 1000) / 1000;
}

		